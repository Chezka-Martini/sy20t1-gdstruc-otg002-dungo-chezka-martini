package com.company;

public class Main {

    public static void main(String[] args) {


        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 50);
        Player hpDeskJet = new Player(3, "HpDeskJet", 80);
        Player heathcliff = new Player(4, "Heathcliff", 99);

        PlayerLinkedLists playerLinkedLists = new PlayerLinkedLists();

        playerLinkedLists.addtoFront(hpDeskJet);
        playerLinkedLists.addtoFront(lethalBacon);
        playerLinkedLists.addtoFront(asuna);

        playerLinkedLists.printPlayerList();

        playerLinkedLists.removePlayer(asuna);
        playerLinkedLists.printPlayerList();

        playerLinkedLists.IndexOf(heathcliff);
        playerLinkedLists.contains(asuna);

    }
}
