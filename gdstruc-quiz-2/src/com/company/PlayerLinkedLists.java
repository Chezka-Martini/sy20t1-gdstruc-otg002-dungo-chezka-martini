package com.company;

import java.util.ArrayList;
import java.util.List;

public class PlayerLinkedLists {
    private PlayerNode head;
    int size = 0;
    //List<Player> playerInfo = new ArrayList<>(size);

    public void addtoFront(Player player)
    {
        //playerInfo.add(player);
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
        size++;

        for(int i = 0; i < size; i++)
        {
            player.setIndex(i);
        }
    }

    public void removePlayer(Player player)
    {
       PlayerNode nextPlayerHead = null;
       PlayerNode temporary = new PlayerNode (player);
       temporary = head; //store the head first so as not to delete it

       if(temporary != null && temporary == head) //change the head of the linked list
       {
           head = temporary.getNextPlayer();
       }

       while(temporary != null && temporary != head) //search for the player to be deleted
       {
           nextPlayerHead = temporary;
           temporary = temporary.getNextPlayer();
       }

       nextPlayerHead.setNextPlayer(temporary.getNextPlayer()); //unlink the playerNode from the list and set the next.
        //playerNode in front as the head.

        size--;
        //playerInfo.remove(player);
        System.out.println("\nPlayer " + player.getName() + " is deleted.");
    }

    public void printPlayerList()
    {
        PlayerNode current = head;
        System.out.print("\n Head -> ");

        while (current != null)
        {
            System.out.println(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
        System.out.println("\n" + "number of Players in the list: " + size);
    }

 /*   public void IndexOf(Player player)
    {
        System.out.println("\nindex of player " + player.getName() + " is: " + playerInfo.indexOf(player));
    }*/

    public void IndexOf(Player player)
    {
        PlayerNode compare = head;

        while(compare != null)
        {
            compare.getPlayer();
            if(player == compare.getPlayer())
            {
                if(player.getIndex() == compare.getPlayer().getIndex())
                {
                    if(player.getId() == compare.getPlayer().getId() && player.getName() == compare.getPlayer().getName() && player.getLevel() ==  compare.getPlayer().getLevel())
                    {
                        System.out.println("\nindex of player " + player.getName() + " is: " + (player.getIndex()));
                    }
                }
            }else compare = compare.getNextPlayer();

        }
        System.out.println("\nindex of player " + player.getName() + " is: -1 ");
    }

    public void contains(Player player)
    {
        PlayerNode compare = head;

        while(compare != null)
        {
            compare.getPlayer();
            if(player == compare.getPlayer())
            {
                if(player.getIndex() == compare.getPlayer().getIndex())
                {
                    if(player.getId() == compare.getPlayer().getId() && player.getName() == compare.getPlayer().getName() && player.getLevel() ==  compare.getPlayer().getLevel())
                    {
                        System.out.println("\nIs player " + player.getName() + " in the list?" + "\nAnswer: true");
                    }
                }
            }else compare = compare.getNextPlayer();

        }
        System.out.println("\nIs player " + player.getName() + " in the list?" + "\nAnswer: false");
        //System.out.println("\nIs player " + player.getName() + " in the list?" + "\nAnswer: " + playerInfo.contains(player));
    }
}
