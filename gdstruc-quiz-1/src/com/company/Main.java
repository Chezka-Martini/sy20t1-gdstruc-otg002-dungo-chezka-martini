package com.company;

public class Main {

    public static void main(String[] args)
    {

        int [] num = new int[10];

        num[0] = 20;
        num[1] = 80;
        num[2] = 73;
        num[3] = 10;
        num[4] = 39;
        num[5] = 68;
        num[6] = 199;
        num[7] = 394;
        num[8] = 274;
        num[9] = 55;

        System.out.println("\nNumbers before sorting: ");
        printArrayElements(num);

        System.out.println("\n\nNumbers after sorting with selection sort: ");
        selectionSort(num);
        printArrayElements(num);

        System.out.println("\n\nNumbers after sorting with bubble sort: ");
        bubbleSort(num);
        printArrayElements(num);

    }

    private static void bubbleSort (int array[])
    {
        for(int lastIndex = array.length - 1 ; lastIndex > 0; lastIndex--)
        {
            for (int i = 0; i < lastIndex; i++)
            {
                if (array[i] < array[i+1])
                {
                        int swap = array[i];
                        array[i] = array[i + 1];
                        array[i+1] = swap;
                }
            }
        }

    }

    private static void selectionSort(int array[])
    {
        for(int lastIndex = array.length - 1 ; lastIndex > 0; lastIndex--)
        {
            int smallestIndex = 0;

            for(int i = 0; i <= lastIndex; i++)
            {
                if (array[i] < array[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int swap = array[lastIndex];
            array[lastIndex] = array[smallestIndex];
            array[smallestIndex] = swap;
        }

    }

    private static void printArrayElements (int array[])
    {

        for (int j : array)
        {
            System.out.print(j + " ");
        }
    }
}
