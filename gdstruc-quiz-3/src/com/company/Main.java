package com.company;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        ArrayQueue queue = new ArrayQueue(7);

        int playerId = 0;
        int playerQueue = 10;
        while(playerQueue != 0)
        {
            int queueCount = 0;
            int rand = (int)(Math.random() * (7 - 1 + 1) + 1);

            System.out.println("Number of Players needed for queue: " + rand + "\n\n\n"); //checker for RNG

            for (int j = 0; j < rand; j++) {

                addingPlayer(queue, playerId);
                playerId++;
            }

            System.out.println("Players still in queue: \n");
            if(queue.size() == 0)
            {
                System.out.println("All players have entered battle.");
            }
            else queue.printQueue();
            scanner.nextLine();

            if(queue.size() >= 5)
            {
                System.out.println("Queue Full. Players will begin to enter battle.");
                removingPlayer(queue, 5);
                playerQueue--;
                System.out.println("Number of queues left to match: " + playerQueue);

            }

            if(playerQueue == 0)
            {
                System.out.println("Congratulations! You have completed 10 queues. \n\n\n" +
                        "Thank you for playing matchmaking simulator");
            }
        }
    }

    public static void addingPlayer(ArrayQueue queue, int id)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter player name for player " + id + " : ");
        String name = scanner.nextLine();

        queue.enqueue(new Player(id, name));
    }

    public static void removingPlayer(ArrayQueue queue, int value)
    {
        for (int i = 0; i < value; i++)
        {
            System.out.println("Player exiting queue: " + queue.dequeue());
        }
    }
}
