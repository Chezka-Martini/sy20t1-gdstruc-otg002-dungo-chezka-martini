package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedStack playerDeck = new LinkedStack();
        ArrayList<Cards> playerHand = new ArrayList<>(30);
        LinkedStack discardedPile = new LinkedStack();

        NamingCards(playerDeck);

        while(playerDeck.stackSize() != -1)
        {
            CommandLists(playerDeck, playerHand, discardedPile);
            printList(playerHand);

            System.out.println("Number of cards left in the deck: " + playerDeck.stackSize());
            System.out.println("Number of cards in the discard pile: " + discardedPile.stackSize());

            if(playerDeck.isEmpty())
            {
                System.out.println("Congratulations, and thank you for playing!!\n");
            }
            scanner.nextLine();
        }

    }


    public static void NamingCards(LinkedStack stack)
    {
        stack.push(new Cards("Warrior")); //1
        stack.push(new Cards("Dragoon"));
        stack.push(new Cards("White Mage"));
        stack.push(new Cards("Monk"));
        stack.push(new Cards("Black Mage"));
        stack.push(new Cards("Thief"));
        stack.push(new Cards("Ranger"));
        stack.push(new Cards("Red Mage"));
        stack.push(new Cards("Blue Mage"));
        stack.push(new Cards("Bard"));
        stack.push(new Cards("Dark Knight"));
        stack.push(new Cards("Paladin"));
        stack.push(new Cards("Alchemist"));
        stack.push(new Cards("Dancer"));
        stack.push(new Cards("Summoner"));
        stack.push(new Cards("Gunner"));
        stack.push(new Cards("Gladiator"));
        stack.push(new Cards("Freelancer"));
        stack.push(new Cards("Berserker"));
        stack.push(new Cards("Time Mage"));
        stack.push(new Cards("Mime"));
        stack.push(new Cards("Mystic Knight"));
        stack.push(new Cards("Ninja"));
        stack.push(new Cards("Geomancer"));//24
        stack.push(new Cards("Samurai"));
        stack.push(new Cards("Necromancer"));
        stack.push(new Cards("Cannoneer"));
        stack.push(new Cards("Oracle"));
        stack.push(new Cards("Scholar"));
        stack.push(new Cards("Machinist"));//30
    }

    public static void printList(ArrayList playerHand)
    {
        if(playerHand.isEmpty())
        {
            System.out.println("You do not have a card in you hand.");
        }
        else System.out.println("Card/s in your hands: " + "\n" + playerHand + "\n");

    }

    public static void CommandLists(LinkedStack stack, ArrayList playerHand, LinkedStack dicardedPile)
    {
        int rand = (int)(Math.random()*(3 - 1 + 1) + 1);
        int value = (int)(Math.random()*(5 - 1 + 1) + 1);

        System.out.println("Program commands you to: \n");
        if (rand == 1)
        {
            System.out.println("Draw " + value + " card/s");
            DrawCard(value, stack, playerHand);
        }
        else if(rand == 2)
        {
            System.out.println("Discard " +  value + " card/s");
            DiscardCard(value, playerHand, dicardedPile);
        }
        else if(rand == 3)
        {
            System.out.println("Get " + value + " card/s from the discarded pile.");
            GetDiscardedCard(value, dicardedPile, playerHand);
        }

    }

    public static void DrawCard(int value, LinkedStack stack, ArrayList hand)
    {
        for (int i = 1; i <= value; i++)
        {
            hand.add(stack.pop());

            if(stack.isEmpty())
            {
                break;
            }
        }
        System.out.println("You draw " + hand);
    }

    public static void DiscardCard(int value, ArrayList hand, LinkedStack discardedPile)
    {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i <= value; i++)
        {
            if(hand.isEmpty())
            {
                System.out.println("You have no cards left in your hand."); break;
            }
            else if (i == value)
            {
                break;
            }
            else System.out.println("Please type the name of the card you are about to discard: "); String cardName = scanner.nextLine();

            Cards temp = new Cards(cardName);
            System.out.println("You discarded " + cardName);
            discardedPile.push(temp);
            hand.remove(temp);
        }
    }

    public static void GetDiscardedCard(int value, LinkedStack discardedPile, ArrayList hand)
    {
        for (int i = 0; i < value; i++)
        {
            if(discardedPile.isEmpty())
            {
                System.out.println("The discard pile is empty.");
                break;
            }
            hand.add(discardedPile.pop());
        }
    }
}
