package com.company;

import java.util.LinkedList;

public class LinkedStack {

    private LinkedList<Cards> cardStack;

    public LinkedStack()
    {
        cardStack = new LinkedList<Cards>();
    }

    public void push(Cards card)
    {
        cardStack.push(card);
    }

    public boolean isEmpty()
    {
        return cardStack.isEmpty();
    }

    public Cards pop()
    {
        return cardStack.pop();
    }

    public int stackSize()
    {
        return cardStack.size();
    }
}
