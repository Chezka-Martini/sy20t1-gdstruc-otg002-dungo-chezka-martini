package com.company;

import java.util.Objects;

public class Player {
    private String username;
    private int id;
    private int level;


    public Player(String username, int id, int level) {
        this.username = username;
        this.id = id;
        this.level = level;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id == player.id &&
                level == player.level &&
                Objects.equals(username, player.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, id, level);
    }

    @Override
    public String toString() {
        return "Player{" +
                "username = '" + username + '\'' +
                ", id = " + id +
                ", level = " + level +
                '}';
    }
}
