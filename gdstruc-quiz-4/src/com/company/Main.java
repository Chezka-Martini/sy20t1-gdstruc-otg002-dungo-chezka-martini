package com.company;

public class Main {

    public static void main(String[] args) {

        Player ploo = new Player ("Plooful", 134, 135);
        Player wardell = new Player ("TSM Wardell", 536, 648);
        Player deadlyJimmy = new Player("DeadlyJimmy", 32, 34);
        Player subroza =  new Player ("Subroza", 4931, 604);
        Player annieDro = new Player ("C9 Annie", 6919, 593);

        PlayerHashTable hashTable = new PlayerHashTable();
        hashTable.put(ploo.getUsername(), ploo);
        hashTable.put(wardell.getUsername(), wardell);
        hashTable.put(deadlyJimmy.getUsername(), deadlyJimmy);
        hashTable.put(subroza.getUsername(), subroza);
        hashTable.put(annieDro.getUsername(), annieDro);


        hashTable.printHashtable();

        System.out.println("\n" + hashTable.get("C9 Annie"));

        hashTable.remove("FNC Tristana");
        hashTable.remove("DeadlyJimmy");

        System.out.println("\n");
        hashTable.printHashtable();
    }
}
